package um.fik.ikm.dmpersonalscreening.beranda


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_beranda.*
import um.fik.ikm.dmpersonalscreening.R

/**
 * A simple [Fragment] subclass.
 *
 */
class BerandaFragment : Fragment() {

    private lateinit var clickListener: ButtonDeteksiClickListener

    companion object {
        fun newInstance() = BerandaFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ButtonDeteksiClickListener)
            clickListener = context
        else
            throw ClassCastException("$context must implement ButtonDeteksiClickListener")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_beranda, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button_deteksi.setOnClickListener { clickListener.onButtonDeteksiClick() }
    }

    interface ButtonDeteksiClickListener {
        fun onButtonDeteksiClick()
    }
}
