package um.fik.ikm.dmpersonalscreening.konsultasi.contact

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_history.view.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.User

class ContactRvAdapter(
        private val onItemClick: ((User) -> Unit),
        private val onImageClick: ((User) -> Unit)
) : androidx.recyclerview.widget.RecyclerView.Adapter<ContactRvAdapter.ViewHolder>() {

    var list: List<User> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context)
                .inflate(R.layout.item_history, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) = p0.fetch(list[p1])

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(user: User) {
            itemView.text_nama.text = user.name

            val subtitle =
                    if (user.role == UserManager.USER_COMMON_ROLE) {
                        if (user.usia.isNotBlank()) "${user.jenisKelamin} ${user.usia}"
                        else ""
                    } else user.role
            itemView.text_pesan.text = subtitle
            itemView.text_pesan.visibility = if (subtitle.isEmpty()) View.GONE else View.VISIBLE

            if (user.status == UserManager.USER_ONLINE) {
                itemView.text_date.visibility = View.VISIBLE
                itemView.text_date.setText(R.string.msg_user_online)
                itemView.text_date.setTextColor(ResourcesCompat.getColor(itemView.resources, R.color.colorAccent, itemView.context.theme))
            } else {
                itemView.text_date.visibility = View.GONE
            }

            if (user.imageUrl.isEmpty()) {
                itemView.image_profile.setImageResource(R.drawable.ic_face_gray_64dp)
            } else {
                Glide.with(itemView)
                        .load(user.imageUrl)
                        .into(itemView.image_profile)
            }

            itemView.image_profile.setOnClickListener { onImageClick(user) }
            itemView.root.setOnClickListener { onItemClick(user) }
        }
    }
}