package um.fik.ikm.dmpersonalscreening.deteksi.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.child_fragment_deteksi.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.deteksi.DeteksiActivity
import um.fik.ikm.dmpersonalscreening.deteksi.DeteksiContract
import um.fik.ikm.dmpersonalscreening.deteksi.LoadQuestListTask
import um.fik.ikm.dmpersonalscreening.deteksi.adapter.ChildFragmentRvAdapter
import um.fik.ikm.dmpersonalscreening.model.Quest
import java.util.*

/**
 * A simple [Fragment] subclass.
 *
 */
class AspekPolaMakanFragment : Fragment(), ChildFragmentRvAdapter.OnItemClickListener, DeteksiActivity.FragmentListener {

    private lateinit var parentView: DeteksiContract.View
    private lateinit var adapter: ChildFragmentRvAdapter
    private val questList: ArrayList<Quest> = ArrayList()

    companion object {
        fun newInstance(list: List<Quest>): AspekPolaMakanFragment {
            val obj = AspekPolaMakanFragment()
            obj.questList.addAll(list)
            return obj
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is DeteksiContract.View) parentView = context
        else throw ClassCastException("$context must implement DeteksiContract.View")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.child_fragment_deteksi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ChildFragmentRvAdapter(
                questList,
                questList.isEmpty(),
                this
        )

        list_quests.adapter = adapter
        list_quests.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)

        if (questList.isEmpty()) {
            LoadQuestListTask(adapter, resources).execute(R.array.questions_3, R.array.score_3)
        }
    }

    override fun onItemClick(score: Int) {
        parentView.onScoreUpdate(score)
    }

    override fun getQuestList() = adapter.getQuestList()
}
