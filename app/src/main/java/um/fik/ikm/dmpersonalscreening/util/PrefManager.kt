package um.fik.ikm.dmpersonalscreening.util

import android.content.Context
import android.content.SharedPreferences
import um.fik.ikm.dmpersonalscreening.profil.model.Profil

class PrefManager(val context: Context) {

    object PrefManager {
        const val SHARED_PREF_NAME = "dm_personal_screening"

        const val PREF_KEY_NAMA = "profil_nama"
        const val PREF_KEY_USIA = "profil_usia"
        const val PREF_KEY_JENIS_KELAMIN = "profil_jenis_kelamin"
        const val PREF_KEY_ALAMAT = "profil_alamat"
        const val PREF_KEY_PEKERJAAN = "profil_pekerjaan"
        const val PREF_KEY_PENDIDIKAN = "profil_pendidikan"
        const val PREF_KEY_DETEKSI = "profil_deteksi"
    }

    private val sharedPref: SharedPreferences

    init {
        sharedPref = context.getSharedPreferences(PrefManager.SHARED_PREF_NAME, Context.MODE_PRIVATE)
    }

    fun saveUserProfil(profil: Profil) {
        val prefEditor = sharedPref.edit()

        prefEditor.putString(PrefManager.PREF_KEY_NAMA, profil.nama)
        prefEditor.putString(PrefManager.PREF_KEY_USIA, profil.usia)
        prefEditor.putString(PrefManager.PREF_KEY_JENIS_KELAMIN, profil.jenisKelamin)
        prefEditor.putString(PrefManager.PREF_KEY_ALAMAT, profil.alamat)
        prefEditor.putString(PrefManager.PREF_KEY_PEKERJAAN, profil.pekerjaan)
        prefEditor.putString(PrefManager.PREF_KEY_PENDIDIKAN, profil.pendidikan)
        prefEditor.putInt(PrefManager.PREF_KEY_DETEKSI, profil.deteksiMode)

        prefEditor.apply()
    }

    fun getProfilNama(): String { return sharedPref.getString(PrefManager.PREF_KEY_NAMA, "") }
    fun getProfilUsia(): String { return sharedPref.getString(PrefManager.PREF_KEY_USIA, "") }
    fun getProfilJenisKelamin(): String { return sharedPref.getString(PrefManager.PREF_KEY_JENIS_KELAMIN, "") }
    fun getProfilAlamat(): String { return sharedPref.getString(PrefManager.PREF_KEY_ALAMAT, "") }
    fun getProfilPekerjaan(): String { return sharedPref.getString(PrefManager.PREF_KEY_PEKERJAAN, "") }
    fun getProfilPendidikan(): String { return sharedPref.getString(PrefManager.PREF_KEY_PENDIDIKAN, "") }
    fun getProfilDeteksi(): Int { return sharedPref.getInt(PrefManager.PREF_KEY_DETEKSI, -1) }
    fun updateProfilDeteksi(newDeteksi: Int) {
        val prefEditor = sharedPref.edit()
        prefEditor.putInt(PrefManager.PREF_KEY_DETEKSI, newDeteksi)
        prefEditor.apply()
    }
}