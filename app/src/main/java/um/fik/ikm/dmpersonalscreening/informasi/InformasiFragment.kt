package um.fik.ikm.dmpersonalscreening.informasi


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_informasi_chat.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.informasi.detail.DetailInfoActivity

/**
 * A simple [Fragment] subclass.
 *
 */
class InformasiFragment : androidx.fragment.app.Fragment() {

    /*private val rvAdapter = InformasiRvAdapter { gotoChatPage(it.user) }*/

    companion object {
        fun newInstance() = InformasiFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_informasi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val intent = Intent(context, DetailInfoActivity::class.java)
        card_gizi.setOnClickListener {
            intent.putExtra("mode", DetailInfoActivity.Constants.MODE_GIZI)
            startActivity(intent)
        }
        card_fisik.setOnClickListener {
            intent.putExtra("mode", DetailInfoActivity.Constants.MODE_FISIK)
            startActivity(intent)
        }
        card_umum.setOnClickListener {
            intent.putExtra("mode", DetailInfoActivity.Constants.MODE_UMUM)
            startActivity(intent)
        }
        card_gigi.setOnClickListener {
            intent.putExtra("mode", DetailInfoActivity.Constants.MODE_GIGI)
            startActivity(intent)
        }

        /*with(swipe_refresh) {
            setColorSchemeResources(R.color.colorAccent)
            setOnRefreshListener { loadMessageHistory() }
        }
        list_chat.adapter = rvAdapter

        val manager = InjectorUtils.provideUserManager()
        val role = manager.getActiveUser().role
        text_chat.setText(
                if (role == "pengguna") R.string.text_chat_ahli
                else R.string.text_inbox
        )*/
    }

    /*override fun onStart() {
        super.onStart()
        loadMessageHistory()
    }

    private fun loadMessageHistory() {
        swipe_refresh.isRefreshing = true
        val manager = InjectorUtils.provideMessageManager()
        manager.getMessageHistoryList { list, error ->
            swipe_refresh.isRefreshing = false
            list?.let {
                rvAdapter.list = it
                text_empty.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
            }
            error?.let { Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show() }
        }
    }

    private fun gotoChatPage(user: User) {
        startActivity(ChatActivity.intent(requireContext(), user))
    }*/
}
