package um.fik.ikm.dmpersonalscreening.deteksi


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_deteksi.*
import um.fik.ikm.dmpersonalscreening.R

/**
 * A simple [Fragment] subclass.
 *
 */

class DeteksiFragment : Fragment() {

    companion object {
        fun newInstance() = DeteksiFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_deteksi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_mulai.setOnClickListener { startActivity(DeteksiActivity.intent(requireContext())) }
    }
}
