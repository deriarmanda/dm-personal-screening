package um.fik.ikm.dmpersonalscreening.konsultasi


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_konsultasi.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.konsultasi.chat.ChatActivity
import um.fik.ikm.dmpersonalscreening.konsultasi.contact.ContactActivity
import um.fik.ikm.dmpersonalscreening.model.MessageHistory
import um.fik.ikm.dmpersonalscreening.model.User
import um.fik.ikm.dmpersonalscreening.profil.ProfilActivity
import um.fik.ikm.dmpersonalscreening.util.InjectorUtils

/**
 * A simple [Fragment] subclass.
 *
 */
class KonsultasiFragment : Fragment(), KonsultasiContract.View {

    override lateinit var presenter: KonsultasiContract.Presenter
    private val rvAdapter = KonsultasiRvAdapter(
            { showChatPage(it.user) },
            { showProfilPage(it) }
    )

    companion object {
        fun newInstance() = KonsultasiFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        presenter = KonsultasiPresenter(
                this,
                InjectorUtils.provideMessageManager(),
                InjectorUtils.provideUserManager()
        )
        return inflater.inflate(R.layout.fragment_konsultasi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(swipe_refresh) {
            setColorSchemeResources(R.color.colorAccent)
            setOnRefreshListener { presenter.loadChatHistory() }
        }
        fab_new_chat.setOnClickListener { showContactPage() }
        list_person.adapter = rvAdapter
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun showChatHistoryList(list: List<MessageHistory>) {
        text_empty.visibility = View.GONE
        list_person.visibility = View.VISIBLE
        rvAdapter.list = list.sortedByDescending { it.lastMessage.timestamp }
    }

    override fun showEmptyHistoryMessage() {
        text_empty.visibility = View.VISIBLE
        list_person.visibility = View.GONE
    }

    override fun showChatPage(user: User) {
        startActivity(ChatActivity.intent(requireContext(), user))
    }

    override fun showProfilPage(user: User) {
        startActivity(ProfilActivity.intent(requireContext(), user))
    }

    override fun showContactPage() {
        startActivity(ContactActivity.intent(requireContext()))
    }

    override fun onLoading(active: Boolean) {
        swipe_refresh.isRefreshing = active
    }

    override fun onError(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

}
