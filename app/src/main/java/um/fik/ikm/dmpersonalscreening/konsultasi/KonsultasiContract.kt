package um.fik.ikm.dmpersonalscreening.konsultasi

import um.fik.ikm.dmpersonalscreening.BasePresenter
import um.fik.ikm.dmpersonalscreening.BaseView
import um.fik.ikm.dmpersonalscreening.model.MessageHistory
import um.fik.ikm.dmpersonalscreening.model.User

interface KonsultasiContract {

    interface Presenter : BasePresenter {
        fun loadChatHistory()
        fun stop()
    }

    interface View : BaseView<Presenter> {
        fun showChatHistoryList(list: List<MessageHistory>)
        fun showEmptyHistoryMessage()
        fun showChatPage(user: User)
        fun showProfilPage(user: User)
        fun showContactPage()
        fun onLoading(active: Boolean)
        fun onError(message: String)
    }

}