package um.fik.ikm.dmpersonalscreening.konsultasi.chat

import android.net.Uri
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.Message
import um.fik.ikm.dmpersonalscreening.model.User
import java.util.*

class ChatPresenter(
        targetUser: User,
        private val view: ChatContract.View,
        private val activeUser: User
) : ChatContract.Presenter {

    private val messageRef: DatabaseReference
    private val messageChildListener: ChildEventListener
    private val targetUserRef: DatabaseReference
    private val targetUserValueListener: ValueEventListener

    init {
        val db = FirebaseDatabase.getInstance().getReference("/chat")
        messageRef =
                if (activeUser.role == UserManager.USER_COMMON_ROLE) db.child(targetUser.uid).child(activeUser.uid)
                else db.child(activeUser.uid).child(targetUser.uid)

        messageChildListener = object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                /*val message = p0.getValue(Message::class.java)
                if (message == null) view.onError("Gagal menerima pesan baru, silahkan coba lagi.")
                else {
                    message.isSelfMessage = message.role == activeUser.role
                    message.uid = p0.key ?: ""
                    view.onChatAdded(message)
                }*/
            }

            override fun onCancelled(p0: DatabaseError) = view.onError("Gagal menerima pesan baru, silahkan coba lagi.")
            override fun onChildMoved(p0: DataSnapshot, p1: String?) {}
            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                val message = p0.getValue(Message::class.java)
                if (message == null) view.onError("Gagal menerima pesan baru, silahkan coba lagi.")
                else {
                    message.isSelfMessage = message.role == activeUser.role
                    message.uid = p0.key ?: ""
                    view.onChatAdded(message)
                }
            }
            override fun onChildRemoved(p0: DataSnapshot) {}
        }

        targetUserRef = FirebaseDatabase.getInstance()
                .getReference("/users")
                .child(targetUser.uid)
                .child("status")
        targetUserValueListener = object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) = view.changeStatusUser(false)

            override fun onDataChange(p0: DataSnapshot) {
                val status = p0.getValue(Int::class.java)
                if (status == null) view.changeStatusUser(false)
                else view.changeStatusUser(status == UserManager.USER_ONLINE)
            }
        }
    }

    override fun loadAll() {
        view.onChatLoading(true)
        messageRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                view.onChatLoading(false)
                view.onError(p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                val list = arrayListOf<Message>()
                for (child in p0.children) {
                    val message = child.getValue(Message::class.java)
                    if (message == null) view.onError("Gagal menerima pesan baru, silahkan coba lagi.")
                    else {
                        message.isSelfMessage = message.role == activeUser.role
                        message.uid = child.key ?: ""
                        list.add(message)
                    }
                }
                view.onChatLoading(false)
                view.onRetrieveAll(list)
            }
        })
    }

    override fun setListener() {
        messageRef.addChildEventListener(messageChildListener)
        targetUserRef.addValueEventListener(targetUserValueListener)
    }

    override fun removeListener() {
        messageRef.removeEventListener(messageChildListener)
        targetUserRef.removeEventListener(targetUserValueListener)
    }

    override fun pushMessageNode(message: Message, addedImage: Uri?, addedVideo: Uri?) {
        message.role = activeUser.role
        when {
            addedImage != null -> {
                view.onUploadLoading(true)
                val storageRef = FirebaseStorage.getInstance().getReference("image/${generateUploadedFileName(addedImage)}")
                storageRef.putFile(addedImage).continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                    if (!task.isSuccessful) {
                        task.exception?.let {
                            throw it
                        }
                    }
                    return@Continuation storageRef.downloadUrl
                }).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        message.imageUrl = task.result?.toString() ?: ""
                        messageRef.push().setValue(message) { dbError, ref ->
                            if (dbError == null) {
                                ref.child("timestamp").setValue(ServerValue.TIMESTAMP) { _, _ ->
                                    view.onSendMessageSuccess()
                                }
                            }
                            else view.onError("Gagal mengirim pesan : ${dbError.message}")
                        }
                    } else {
                        view.onUploadLoading(false)
                        view.onError("Gagal mengunggah gambar : ${task.exception?.message}")
                    }
                }
            }
            addedVideo != null -> {
                view.onUploadLoading(true)
                val storageRef = FirebaseStorage.getInstance().getReference("video/${generateUploadedFileName(addedVideo)}")
                storageRef.putFile(addedVideo).continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                    if (!task.isSuccessful) {
                        task.exception?.let {
                            throw it
                        }
                    }
                    return@Continuation storageRef.downloadUrl
                }).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        message.videoUrl = task.result?.toString() ?: ""
                        messageRef.push().setValue(message) { dbError, ref ->
                            if (dbError == null) {
                                ref.child("timestamp").setValue(ServerValue.TIMESTAMP) { _, _ ->
                                    view.onSendMessageSuccess()
                                }
                            }
                            else view.onError("Gagal mengirim pesan : ${dbError.message}")
                        }
                    } else {
                        view.onUploadLoading(false)
                        view.onError("Gagal mengunggah video : ${task.exception?.message}")
                    }
                }
            }
            else -> messageRef.push().setValue(message) { dbError, ref ->
                if (dbError == null) {
                    ref.child("timestamp").setValue(ServerValue.TIMESTAMP) { _, _ ->
                        view.onSendMessageSuccess()
                    }
                }
                else view.onError("Gagal mengirim pesan : ${dbError.message}")
            }
        }
    }

    private fun generateUploadedFileName(uri: Uri): String {
        val time = Date().time
        return "${uri.lastPathSegment}-$time"
    }
}