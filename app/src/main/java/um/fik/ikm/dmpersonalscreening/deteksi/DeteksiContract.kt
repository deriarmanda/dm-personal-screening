package um.fik.ikm.dmpersonalscreening.deteksi

import um.fik.ikm.dmpersonalscreening.BasePresenter
import um.fik.ikm.dmpersonalscreening.BaseView
import um.fik.ikm.dmpersonalscreening.model.Quest

interface DeteksiContract {

    interface Presenter : BasePresenter {
        fun prepareRiwayatDeteksi()
        fun updateScore(score: Int)
        fun calculateScore(answerMap: HashMap<String, List<Quest>>)
    }

    interface View : BaseView<Presenter> {
        fun onShowKuisioner()
        fun onShowRiwayatDeteksi(map: Map<String, List<Quest>>)
        fun onShowRiwayatError(message: String)
        fun onScoreUpdate(score: Int)
        fun submitScore()
        fun showResultPage(hasil: Int)
        fun onError(message: String)
        fun onLoading(active: Boolean)
    }
}