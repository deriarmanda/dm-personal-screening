package um.fik.ikm.dmpersonalscreening

interface BaseView<T> {
    var presenter: T
}