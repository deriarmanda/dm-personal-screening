package um.fik.ikm.dmpersonalscreening.informasi.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_detail_info.*
import um.fik.ikm.dmpersonalscreening.R

class DetailInfoActivity : AppCompatActivity() {

    object Constants {
        const val MODE_GIZI = 111
        const val MODE_FISIK = 112
        const val MODE_UMUM = 113
        const val MODE_GIGI = 114
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_info)

        val mode = intent.getIntExtra("mode", -1)
        setupViewAndToolbar(mode)
    }

    private fun setupViewAndToolbar(mode: Int) {
        // setup data
        val title: Int
        val icon: Int
        val info: Int

        when (mode) {
            Constants.MODE_GIZI -> {
                title = R.string.title_gizi
                icon = R.drawable.ic_gizi_white_32dp
                info = R.string.text_info_gizi
            }
            Constants.MODE_FISIK -> {
                title = R.string.title_fisik
                icon = R.drawable.ic_aktivitas_fisik_white_32dp
                info = R.string.text_info_fisik
            }
            Constants.MODE_UMUM -> {
                title = R.string.title_umum
                icon = R.drawable.ic_kesehatan_umum_white_32dp
                info = R.string.text_info_umum
            }
            Constants.MODE_GIGI -> {
                title = R.string.title_gigi
                icon = R.drawable.ic_kesehatan_gigi_white_32dp
                info = R.string.text_info_gigi
            }
            else -> return
        }

        // setup view
        text_info.setText(info)
        btn_selesai.setOnClickListener { onBackPressed() }

        // setup toolbar
        toolbar.setTitle(title)
        toolbar.setLogo(icon)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

}
