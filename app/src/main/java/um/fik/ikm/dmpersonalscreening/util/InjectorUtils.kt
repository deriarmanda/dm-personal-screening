package um.fik.ikm.dmpersonalscreening.util

import um.fik.ikm.dmpersonalscreening.konsultasi.chat.ChatContract
import um.fik.ikm.dmpersonalscreening.konsultasi.chat.ChatPresenter
import um.fik.ikm.dmpersonalscreening.manager.MessageManager
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.User

object InjectorUtils {

    fun provideUserManager() = UserManager.getInstance()

    fun provideMessageManager() = MessageManager.getInstance()

    fun provideChatPresenter(targetUser: User, view: ChatContract.View): ChatPresenter {
        val activeUser = provideUserManager().getActiveUser()
        return ChatPresenter(targetUser, view, activeUser)
    }
}