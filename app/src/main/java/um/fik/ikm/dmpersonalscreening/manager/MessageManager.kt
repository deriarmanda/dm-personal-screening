package um.fik.ikm.dmpersonalscreening.manager

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.fik.ikm.dmpersonalscreening.model.Message

class MessageManager private constructor() {

    private val mDatabase = FirebaseDatabase.getInstance().getReference("/chat")

    companion object {
        private const val TAG = "MessageManager"
        @Volatile
        private var instance: MessageManager? = null

        fun getInstance(): MessageManager {
            return instance ?: synchronized(this) {
                instance ?: MessageManager().also { instance = it }
            }
        }
    }

    fun getUserHistoryList(uidPengguna: String, callback: (list: List<String>?, error: String?) -> Unit) {
        mDatabase.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) = callback(null, p0.message)
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val list = arrayListOf<String>()
                    for (uidAhli in snapshot.children) {
                        for (uid in uidAhli.children) {
                            if (uid.key == uidPengguna) {
                                list.add(uidAhli.key!!)
                                break
                            }
                        }
                    }
                    callback(list, null)
                } else callback(emptyList(), null)
            }
        })
    }

    fun getAhliHistoryList(uidAhli: String, callback: (list: List<String>?, error: String?) -> Unit) {
        val dbRef = mDatabase.child(uidAhli)
        dbRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) = callback(null, p0.message)
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val list = arrayListOf<String>()
                    for (uid in snapshot.children) {
//                        if (uid.key == uidAhli) {
                        list.add(uid.key!!)
//                            break
//                        }
                    }
                    callback(list, null)
                } else callback(emptyList(), null)
            }
        })
    }

    fun findLatestMessage(
            uidAhli: String,
            uidPengguna: String,
            callback: ((message: Message?) -> Unit)
    ) {
        val dbRef = mDatabase.child(uidAhli).child(uidPengguna)
                .orderByChild("timestamp")
                .limitToLast(1)
        dbRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) = callback(null)

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    for (childs in p0.children) {
                        val msg = childs.getValue(Message::class.java)
                        if (msg != null) {
                            msg.uid = childs.key ?: ""
                            callback(msg)
                        } else callback(null)
                    }
                } else callback(null)
            }
        })
    }
}