package um.fik.ikm.dmpersonalscreening.konsultasi.chat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_chat_left.view.*
import kotlinx.android.synthetic.main.item_chat_right.view.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.model.Message
import um.fik.ikm.dmpersonalscreening.util.DateTimeUtils

private const val TYPE_LEFT = 1
private const val TYPE_RIGHT = 2

class ChatRvAdapter(
        private val onImageClicked: (imageUrl: String) -> Unit,
        private val onVideoClicked: (videoUrl: String) -> Unit
) : RecyclerView.Adapter<ChatRvAdapter.ViewHolder>() {

    private var list: ArrayList<Message> = arrayListOf()

    override fun getItemViewType(position: Int): Int {
        return if (list[position].isSelfMessage) TYPE_RIGHT
        else TYPE_LEFT
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return if (p1 == TYPE_RIGHT) {
            val view = LayoutInflater.from(p0.context)
                    .inflate(R.layout.item_chat_right, p0, false)
            RightViewHolder(view)
        } else {
            val view = LayoutInflater.from(p0.context)
                    .inflate(R.layout.item_chat_left, p0, false)
            LeftViewHolder(view)
        }
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val message = list[p1]
        if (message.isSelfMessage) (p0 as RightViewHolder).fetch(message)
        else (p0 as LeftViewHolder).fetch(message)
    }

    fun setMessages(messages: List<Message>) {
        list.clear()
        list.addAll(messages)
        notifyDataSetChanged()
    }

    fun addMessage(message: Message) {
        if (!list.contains(message)) {
            list.add(message)
            notifyItemInserted(list.indexOf(message))
        } else {
            val index = list.indexOf(message)
            list[index] = message
            notifyItemChanged(index)
        }
    }

    open class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    inner class LeftViewHolder(view: View) : ViewHolder(view) {
        fun fetch(message: Message) {
            itemView.text_message_left.text = message.message
            when {
                message.imageUrl.isNotEmpty() -> {
                    Glide.with(itemView).load(message.imageUrl).into(itemView.image_message_left)
                    itemView.image_message_left.setOnClickListener { onImageClicked(message.imageUrl) }
                    itemView.text_datetime_left_image.text = DateTimeUtils.parseForDisplayedMessage(message.timestamp)
                    itemView.text_datetime_left.visibility = View.GONE
                    itemView.container_video_left.visibility = View.GONE
                    itemView.container_image_left.visibility = View.VISIBLE
                }
                message.videoUrl.isNotEmpty() -> {
                    itemView.box_video_left.setOnClickListener { onVideoClicked(message.videoUrl) }
                    itemView.text_datetime_left_video.text = DateTimeUtils.parseForDisplayedMessage(message.timestamp)
                    itemView.text_datetime_left.visibility = View.GONE
                    itemView.container_image_left.visibility = View.GONE
                    itemView.container_video_left.visibility = View.VISIBLE
                }
                else -> {
                    itemView.container_image_left.visibility = View.GONE
                    itemView.container_video_left.visibility = View.GONE
                    itemView.text_datetime_left.text = DateTimeUtils.parseForDisplayedMessage(message.timestamp)
                }
            }
        }
    }

    inner class RightViewHolder(view: View) : ViewHolder(view) {
        fun fetch(message: Message) {
            itemView.text_message_right.text = message.message
            when {
                message.imageUrl.isNotEmpty() -> {
                    Glide.with(itemView).load(message.imageUrl).into(itemView.image_message_right)
                    itemView.image_message_right.setOnClickListener { onImageClicked(message.imageUrl) }
                    itemView.text_datetime_right_image.text = DateTimeUtils.parseForDisplayedMessage(message.timestamp)
                    itemView.text_datetime_right.visibility = View.GONE
                    itemView.container_video_right.visibility = View.GONE
                    itemView.container_image_right.visibility = View.VISIBLE
                }
                message.videoUrl.isNotEmpty() -> {
                    itemView.box_video_right.setOnClickListener { onVideoClicked(message.videoUrl) }
                    itemView.text_datetime_right_video.text = DateTimeUtils.parseForDisplayedMessage(message.timestamp)
                    itemView.text_datetime_right.visibility = View.GONE
                    itemView.container_image_right.visibility = View.GONE
                    itemView.container_video_right.visibility = View.VISIBLE
                }
                else -> {
                    itemView.container_image_right.visibility = View.GONE
                    itemView.container_video_right.visibility = View.GONE
                    itemView.text_datetime_right.text = DateTimeUtils.parseForDisplayedMessage(message.timestamp)
                }
            }
        }
    }
}