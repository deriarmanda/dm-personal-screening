package um.fik.ikm.dmpersonalscreening.profil


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_profil.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.deteksi.DeteksiActivity
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.User
import um.fik.ikm.dmpersonalscreening.profil.ubah.UbahProfilActivity

/**
 * A simple [Fragment] subclass.
 *
 */
class ProfilFragment : androidx.fragment.app.Fragment(), ProfilContract.View {

    override lateinit var presenter: ProfilContract.Presenter

    companion object {
        fun newInstance() = ProfilFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profil, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = ProfilPresenter(this, UserManager.getInstance())
        fab_edit.setOnClickListener { presenter.editProfil() }
        btn_deteksi.setOnClickListener { presenter.loadDeteksiHistory() }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    /*override fun showEmptyProfil() {
        view_shadow.visibility = View.VISIBLE
        text_edit_profil.visibility = View.VISIBLE
    }*/

    override fun showProfil(user: User) {
        view_shadow.visibility = View.GONE
        text_edit_profil.visibility = View.GONE

        text_nama.text = user.name
        text_usia.text = user.role
        if (user.imageUrl.isNotBlank()) {
            Glide.with(this)
                    .load(user.imageUrl)
                    .into(image_profile)
        } else {
            image_profile.setImageResource(R.drawable.ic_user_blue_104dp)
        }
        if (user.usia.isNotBlank()) {
            text_usia.text = String.format("%s (%s)", user.role, user.usia)
            text_alamat.text = user.alamat
            text_gender.text = user.jenisKelamin
            text_pekerjaan.text = user.pekerjaan
            text_pendidikan.text = user.pendidikan
        }
    }

    override fun showUbahProfilPage() {
        val intent = Intent(context, UbahProfilActivity::class.java)
        startActivity(intent)
    }

    override fun showDeteksiHistory() {
        btn_deteksi.visibility = View.VISIBLE
    }

    override fun showEmptyDeteksiHistory() {
        btn_deteksi.visibility = View.GONE
    }

    override fun showHasilDeteksiPage(user: User) {
        startActivity(DeteksiActivity.intent(requireContext(), user, false))
    }
}
