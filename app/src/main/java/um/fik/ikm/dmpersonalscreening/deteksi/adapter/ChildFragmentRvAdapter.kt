package um.fik.ikm.dmpersonalscreening.deteksi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_deteksi_checkbox.view.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.model.Quest

class ChildFragmentRvAdapter(
        private var questList: List<Quest>,
        private val isEditable: Boolean,
        private val itemClickListener: OnItemClickListener
) : RecyclerView.Adapter<ChildFragmentRvAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_deteksi_checkbox, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fetch(questList[position])
    }

    override fun getItemCount(): Int = questList.size

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemViewType(position: Int): Int = position

    fun updateQuestList(newList: List<Quest>?) {
        questList = newList ?: questList
        notifyDataSetChanged()
    }

    fun getQuestList() = questList

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        fun fetch(quest: Quest) {
            itemView.checkbox_quest.text = quest.question

            if (isEditable) {
                itemView.checkbox_quest.isClickable = true
                itemView.checkbox_quest.setOnCheckedChangeListener { _, checked ->
                    val score =
                            if (checked) quest.score
                            else {
                                if (quest.alreadyChecked) quest.score * -1
                                else 0
                            }

                    quest.answer = if (checked) "ya" else "tidak"
                    quest.alreadyChecked = true
                    itemClickListener.onItemClick(score)
                }
            } else {
                itemView.checkbox_quest.isClickable = false
                itemView.checkbox_quest.isChecked = quest.answer == "ya"
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(score: Int)
    }
}