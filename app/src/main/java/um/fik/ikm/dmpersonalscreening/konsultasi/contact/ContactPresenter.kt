package um.fik.ikm.dmpersonalscreening.konsultasi.contact

import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.User

class ContactPresenter(
        private val view: ContactContract.View,
        private val userManager: UserManager
) : ContactContract.Presenter {

    private val currentUser: User

    init {
        view.presenter = this
        currentUser = userManager.getActiveUser()
    }

    override fun loadContactList() {
        view.onLoading(true)
        val role = currentUser.role
        if (role == UserManager.USER_COMMON_ROLE) {
            userManager.getAhliUsersOnly { list, errorMsg ->
                view.onLoading(false)
                if (errorMsg != null) view.onError(errorMsg)
                if (list != null) {
                    view.showToolbarSubtitle("${list.size} Kontak Ahli")
                    view.showContactList(list)
                }
            }
        } else {
            userManager.getAllUsers { list, errorMsg ->
                view.onLoading(false)
                if (errorMsg != null) view.onError(errorMsg)
                if (list != null) {
                    view.showToolbarSubtitle("${list.size} Pengguna Aktif")
                    view.showContactList(list)
                }
            }
        }
    }

    override fun start() = loadContactList()
}