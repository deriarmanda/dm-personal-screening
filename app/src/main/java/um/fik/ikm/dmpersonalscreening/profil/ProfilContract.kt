package um.fik.ikm.dmpersonalscreening.profil

import um.fik.ikm.dmpersonalscreening.BasePresenter
import um.fik.ikm.dmpersonalscreening.BaseView
import um.fik.ikm.dmpersonalscreening.model.User

interface ProfilContract {

    interface Presenter: BasePresenter {
        fun loadProfil()
        fun editProfil()
        fun loadDeteksiHistory()
    }

    interface View: BaseView<Presenter> {
        fun showProfil(user: User)
        fun showDeteksiHistory()
        fun showEmptyDeteksiHistory()
        fun showUbahProfilPage()
        fun showHasilDeteksiPage(user: User)
    }
}