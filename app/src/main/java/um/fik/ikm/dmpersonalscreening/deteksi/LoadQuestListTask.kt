package um.fik.ikm.dmpersonalscreening.deteksi

import android.content.res.Resources
import android.os.AsyncTask
import um.fik.ikm.dmpersonalscreening.deteksi.adapter.ChildFragmentRvAdapter
import um.fik.ikm.dmpersonalscreening.model.Quest
import java.util.*

class LoadQuestListTask(
        private val adapter: ChildFragmentRvAdapter,
        private val resources: Resources,
        private val shuffled: Boolean = true
) : AsyncTask<Int, Void, List<Quest>>() {

    override fun doInBackground(vararg arrayRes: Int?): List<Quest> {
        val questList: ArrayList<Quest> = ArrayList()
        val questions = resources.getStringArray(arrayRes[0]!!)
        val score = resources.getIntArray(arrayRes[1]!!)

        for (index in questions.indices) {
            val quest = Quest(
                    question = questions[index],
                    score = score[index]
            )
            questList.add(quest)
        }

        if (shuffled) questList.shuffle()

        return questList
    }

    override fun onPostExecute(result: List<Quest>?) {
        super.onPostExecute(result)
        adapter.updateQuestList(result)
    }
}