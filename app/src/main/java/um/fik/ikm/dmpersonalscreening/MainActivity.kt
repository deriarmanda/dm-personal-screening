package um.fik.ikm.dmpersonalscreening

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.firebase.ui.auth.AuthUI
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main.*
import um.fik.ikm.dmpersonalscreening.beranda.BerandaFragment
import um.fik.ikm.dmpersonalscreening.deteksi.DeteksiFragment
import um.fik.ikm.dmpersonalscreening.informasi.InformasiFragment
import um.fik.ikm.dmpersonalscreening.konsultasi.KonsultasiFragment
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.panduan.PanduanActivity
import um.fik.ikm.dmpersonalscreening.profil.ProfilFragment
import um.fik.ikm.dmpersonalscreening.util.*

private const val RC_SIGN_IN = 110

class MainActivity : AppCompatActivity(), BerandaFragment.ButtonDeteksiClickListener {

    private lateinit var loadingDialog: AlertDialog
    private lateinit var confirmDialog: AlertDialog
    private lateinit var mAuth: FirebaseAuth
    private lateinit var userManager: UserManager
    private val mAuthStateListener = FirebaseAuth.AuthStateListener { auth ->
        val user = auth.currentUser
        if (user != null) //onUserSignedIn(user)
        else onUserSignedOut()
    }
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val currFragment: androidx.fragment.app.Fragment?

        when (item.itemId) {
            R.id.home_bottom_nav_beranda -> {
                currFragment = supportFragmentManager
                        .findFragmentByTag(TAG_BERANDA) ?: BerandaFragment.newInstance()
                replaceFragmentByTag(R.id.fragment_container, currFragment, TAG_BERANDA)
                toolbar.visibility = View.GONE
                return@OnNavigationItemSelectedListener true
            }
            R.id.home_bottom_nav_info -> {
                currFragment = supportFragmentManager
                        .findFragmentByTag(TAG_INFORMASI) ?: InformasiFragment.newInstance()
                replaceFragmentByTag(R.id.fragment_container, currFragment, TAG_INFORMASI)
                toolbar.title = getString(R.string.title_informasi)
                toolbar.subtitle = null
                toolbar.setLogo(R.drawable.ic_book_white_32dp)
                toolbar.visibility = View.VISIBLE
                return@OnNavigationItemSelectedListener true
            }
            R.id.home_bottom_nav_deteksi -> {
                currFragment = supportFragmentManager
                        .findFragmentByTag(TAG_DETEKSI) ?: DeteksiFragment.newInstance()
                replaceFragmentByTag(R.id.fragment_container, currFragment, TAG_DETEKSI)
                toolbar.title = getString(R.string.title_deteksi)
                toolbar.setLogo(R.drawable.ic_data_white_32dp)
                toolbar.visibility = View.VISIBLE
                return@OnNavigationItemSelectedListener true
            }
            R.id.home_bottom_nav_profil -> {
                currFragment = supportFragmentManager
                        .findFragmentByTag(TAG_PROFIL) ?: ProfilFragment.newInstance()
                replaceFragmentByTag(R.id.fragment_container, currFragment, TAG_PROFIL)
                toolbar.title = getString(R.string.title_profil)
                toolbar.subtitle = null
                //toolbar.setLogo(R.drawable.ic_person_white_32dp)
                toolbar.visibility = View.GONE
                return@OnNavigationItemSelectedListener true
            }
            R.id.home_bottom_nav_konsultasi -> {
                currFragment = supportFragmentManager
                        .findFragmentByTag(TAG_KONSULTASI) ?: KonsultasiFragment.newInstance()
                replaceFragmentByTag(R.id.fragment_container, currFragment, TAG_KONSULTASI)
                toolbar.title = getString(R.string.title_konsultasi)
                toolbar.subtitle = null
                toolbar.setLogo(R.drawable.ic_question_white_32dp)
                toolbar.visibility = View.VISIBLE
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar.setSubtitleTextColor(ResourcesCompat.getColor(resources, R.color.lightGray, theme))
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        userManager = InjectorUtils.provideUserManager()

        mAuth = FirebaseAuth.getInstance()
        mAuth.currentUser?.let { onUserSignedIn(it) }

        bottom_nav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottom_nav.selectedItemId = R.id.home_bottom_nav_beranda

        loadingDialog = AlertDialog.Builder(this)
                .setView(R.layout.dialog_loading)
                .setCancelable(false)
                .create()
        val coloredTitle = SpannableString(getString(R.string.title_confirm))
        coloredTitle.setSpan(
                ForegroundColorSpan(ResourcesCompat.getColor(resources, R.color.colorPrimary, theme)),
                0,
                coloredTitle.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        confirmDialog = AlertDialog.Builder(this)
                .setTitle(coloredTitle)
                .setIcon(R.drawable.ic_confirm_dialog_blue_24dp)
                .setMessage(R.string.msg_confirm_exit)
                .setCancelable(true)
                .setPositiveButton(R.string.btn_ya) { _, _ -> userManager.doLogout() }
                .setNegativeButton(R.string.btn_tidak) { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
                .create()
    }

    override fun onStart() {
        super.onStart()
        mAuth.addAuthStateListener(mAuthStateListener)
    }

    override fun onStop() {
        super.onStop()
        mAuth.removeAuthStateListener(mAuthStateListener)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == Activity.RESULT_OK) {
                mAuth.currentUser?.let { onUserSignedIn(it) }
            } else {
                Toast.makeText(this@MainActivity, "Selamat jumpa kembali.", Toast.LENGTH_SHORT).show()
                finish()
            }
        } else super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        if (mAuth.currentUser != null && bottom_nav.selectedItemId != R.id.home_bottom_nav_beranda) {
            bottom_nav.selectedItemId = R.id.home_bottom_nav_beranda
        } else super.onBackPressed()
    }

    override fun onButtonDeteksiClick() {
        bottom_nav.selectedItemId = R.id.home_bottom_nav_deteksi
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.panduan -> {
                startActivity(Intent(this, PanduanActivity::class.java))
                true
            }
            R.id.logout -> {
                confirmDialog.show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onUserSignedIn(user: FirebaseUser) {
        val currentUser = userManager.getActiveUser()
        if (currentUser.uid.isBlank()) {
            loadingDialog = AlertDialog.Builder(this)
                    .setView(R.layout.dialog_loading)
                    .setCancelable(false)
                    .create()
            loadingDialog.show()
            userManager.loadUserProfile(user) { errorMessage ->
                loadingDialog.hide()
                if (errorMessage != null) {
                    Toast.makeText(this@MainActivity, errorMessage, Toast.LENGTH_LONG).show()
                    finish()
                    //mAuth.signOut()
                } else {
                    userManager.prepareUserStatusListener()
                    Toast.makeText(
                            this@MainActivity,
                            "Selamat datang di DMPersonalScreening App.",
                            Toast.LENGTH_SHORT
                    ).show()
                }
            }
        } else {
            userManager.prepareUserStatusListener()
            Toast.makeText(
                    this@MainActivity,
                    "Selamat datang di DMPersonalScreening App.",
                    Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun onUserSignedOut() {
        val intent = AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(listOf(
                        AuthUI.IdpConfig.GoogleBuilder().build(),
                        AuthUI.IdpConfig.EmailBuilder().build()
                ))
                .setIsSmartLockEnabled(false, true)
                .setLogo(R.drawable.logo_app_full)
                .setTheme(R.style.AppTheme_Login)
                .build()

        startActivityForResult(intent, RC_SIGN_IN)
    }
}
