package um.fik.ikm.dmpersonalscreening.konsultasi.contact

import um.fik.ikm.dmpersonalscreening.BasePresenter
import um.fik.ikm.dmpersonalscreening.BaseView
import um.fik.ikm.dmpersonalscreening.model.User

interface ContactContract {

    interface Presenter : BasePresenter {
        fun loadContactList()
    }

    interface View : BaseView<Presenter> {
        fun showContactList(list: List<User>)
        fun showToolbarSubtitle(message: String)
        fun showChatPage(user: User)
        fun showProfilPage(user: User)
        fun onLoading(active: Boolean)
        fun onError(message: String)
    }
}