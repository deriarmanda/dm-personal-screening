package um.fik.ikm.dmpersonalscreening.profil.model

import android.os.Parcel
import android.os.Parcelable

data class Profil (
        var nama: String,
        var usia: String,
        var jenisKelamin: String,
        var alamat: String,
        var pekerjaan: String,
        var pendidikan: String,
        var deteksiMode: Int
) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt())

    fun isEmpty(): Boolean { return nama.isBlank() }
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nama)
        parcel.writeString(usia)
        parcel.writeString(jenisKelamin)
        parcel.writeString(alamat)
        parcel.writeString(pekerjaan)
        parcel.writeString(pendidikan)
        parcel.writeInt(deteksiMode)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Profil> {
        override fun createFromParcel(parcel: Parcel): Profil {
            return Profil(parcel)
        }

        override fun newArray(size: Int): Array<Profil?> {
            return arrayOfNulls(size)
        }
    }

}