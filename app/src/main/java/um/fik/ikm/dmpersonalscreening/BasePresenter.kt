package um.fik.ikm.dmpersonalscreening

interface BasePresenter {
    fun start()
}