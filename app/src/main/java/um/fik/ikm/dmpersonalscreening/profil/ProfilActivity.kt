package um.fik.ikm.dmpersonalscreening.profil

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_profil.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.deteksi.DeteksiActivity
import um.fik.ikm.dmpersonalscreening.deteksi.hasil.HasilDeteksiActivity.Constants.HASIL_NONE
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.User

class ProfilActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_USER = "extra_user"
        fun intent(context: Context, user: User): Intent {
            val intent = Intent(context, ProfilActivity::class.java)
            intent.putExtra(EXTRA_USER, user)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_profil)

        val user = intent.getParcelableExtra<User>(EXTRA_USER)
        showProfil(user)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun showProfil(user: User) {
        view_shadow.visibility = View.GONE
        text_edit_profil.visibility = View.GONE
        fab_edit.hide()

        supportActionBar?.setTitle(
                if (user.role == UserManager.USER_COMMON_ROLE) R.string.title_profil_pengguna
                else R.string.title_profil_ahli
        )

        text_nama.text = user.name
        text_usia.text = user.role
        btn_deteksi.visibility = if (user.hasilDeteksi != HASIL_NONE) View.VISIBLE else View.GONE
        if (user.imageUrl.isNotBlank()) {
            Glide.with(this)
                    .load(user.imageUrl)
                    .into(image_profile)
        } else {
            image_profile.setImageResource(R.drawable.ic_user_blue_104dp)
        }
        if (user.usia.isNotBlank()) {
            text_usia.text = String.format("%s (%s)", user.role, user.usia)
            text_alamat.text = user.alamat
            text_gender.text = user.jenisKelamin
            text_pekerjaan.text = user.pekerjaan
            text_pendidikan.text = user.pendidikan
        }

        btn_deteksi.setOnClickListener {
            startActivity(DeteksiActivity.intent(this, user, false))
        }
    }
}
