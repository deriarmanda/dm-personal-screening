package um.fik.ikm.dmpersonalscreening.util

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import um.fik.ikm.dmpersonalscreening.deteksi.fragments.AspekFisikFragment
import um.fik.ikm.dmpersonalscreening.deteksi.fragments.AspekGigiFragment
import um.fik.ikm.dmpersonalscreening.deteksi.fragments.AspekPolaMakanFragment
import um.fik.ikm.dmpersonalscreening.deteksi.fragments.AspekUmumFragment
import um.fik.ikm.dmpersonalscreening.model.Quest

const val TAG_BERANDA: String = "beranda"
const val TAG_INFORMASI: String = "info"
const val TAG_DETEKSI: String = "deteksi"
const val TAG_PROFIL: String = "profil"
const val TAG_KONSULTASI: String = "konsultasi"

fun AppCompatActivity.replaceFragmentByTag(@IdRes containerId: Int, fragment: Fragment, tag: String) {
    supportFragmentManager.beginTransaction()
            .replace(containerId, fragment, tag)
            .commit()
}

/*fun AppCompatActivity.showNotImplementedMessage() {
    Toast.makeText(this, "Not implemented yet!", Toast.LENGTH_SHORT).show()
}*/

fun getFormDeteksiFragment(map: Map<String, List<Quest>>): Array<Fragment> = arrayOf(
        AspekUmumFragment.newInstance(map["aspek_umum"] ?: emptyList()),
        AspekGigiFragment.newInstance(map["aspek_gigi"] ?: emptyList()),
        AspekPolaMakanFragment.newInstance(map["aspek_pola_makan"] ?: emptyList()),
        AspekFisikFragment.newInstance(map["aspek_fisik"] ?: emptyList())
)
