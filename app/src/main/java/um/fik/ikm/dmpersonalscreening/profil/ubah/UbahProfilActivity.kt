package um.fik.ikm.dmpersonalscreening.profil.ubah

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_ubah_profil.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.User

class UbahProfilActivity : AppCompatActivity(), UbahProfilContract.View {

    override lateinit var presenter: UbahProfilContract.Presenter
    private lateinit var mUser: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ubah_profil)

        presenter = UbahProfilPresenter(this, UserManager.getInstance())
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.ubah_profil_option_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.simpan -> {
                validateForm()
                true
            }
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun setCurrentProfil(user: User) {
        clearErrors()
        mUser = user

        supportActionBar?.subtitle = mUser.name
        form_nama.setText(mUser.name)
        if (mUser.usia.isNotBlank()) form_usia.setText(mUser.usia.substringBefore(" "))
        radio_laki_laki.isChecked = mUser.jenisKelamin == "Laki - laki"
        radio_perempuan.isChecked = mUser.jenisKelamin == "Perempuan"
        form_alamat.setText(mUser.alamat)
        form_pekerjaan.setText(mUser.pekerjaan)
        form_pendidikan.setText(mUser.pendidikan)

        if (!radio_laki_laki.isChecked && !radio_perempuan.isChecked) radio_laki_laki.isChecked = true
    }

    override fun validateForm() {
        clearErrors()
        var isValid = true

        val nama = form_nama.text.toString()
        val usia = form_usia.text.toString()
        val jenisKelamin = if (radio_laki_laki.isChecked) "Laki - laki" else "Perempuan"
        val alamat = form_alamat.text.toString()
        val pekerjaan = form_pekerjaan.text.toString()
        val pendidikan = form_pendidikan.text.toString()

        if (nama.isBlank()) {
            showError(form_layout_nama)
            isValid = false
        }
        if (usia.isBlank()) {
            showError(form_layout_usia)
            isValid = false
        }
        if (alamat.isBlank()) {
            showError(form_layout_alamat)
            isValid = false
        }
        if (pekerjaan.isBlank()) {
            showError(form_layout_pekerjaan)
            isValid = false
        }
        if (pendidikan.isBlank()) {
            showError(form_layout_pendidikan)
            isValid = false
        }

        if (isValid) {
            mUser.name = nama
            mUser.usia = "$usia Tahun"
            mUser.jenisKelamin = jenisKelamin
            mUser.alamat = alamat
            mUser.pekerjaan = pekerjaan
            mUser.pendidikan = pendidikan

            presenter.saveEditedProfil(mUser)
        } else {
            Toast.makeText(
                    this,
                    "Gagal mengubah profil, kolom harus diisi !",
                    Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onSaveProfilSucceed() {
        Toast.makeText(
                this,
                "Berhasil merubah profil",
                Toast.LENGTH_SHORT
        ).show()
        finish()
    }

    override fun onSaveProfilError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onLoading(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }

    private fun clearErrors() {
        form_layout_nama.isErrorEnabled = false
        form_layout_usia.isErrorEnabled = false
        form_layout_alamat.isErrorEnabled = false
        form_layout_pekerjaan.isErrorEnabled = false
        form_layout_pendidikan.isErrorEnabled = false
    }

    private fun showError(view: TextInputLayout) {
        view.isErrorEnabled = true
        view.error = "Kolom tidak boleh kosong !"
    }
}
