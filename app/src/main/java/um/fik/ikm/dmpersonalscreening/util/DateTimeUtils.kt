package um.fik.ikm.dmpersonalscreening.util

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {

    private const val DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm"
    private const val DAY_DATE_PATTERN = "EEEE, d MMM yyyy"
    private const val DATE_PATTERN = "yyyy-MM-dd"
    private const val TIME_PATTERN = "HH:mm"
    private const val CHAT_DATE_PATTERN = "dd/MM/yy"
    private const val CHAT_DATE_YEARLESS_PATTERN = "dd/MM"
    private val DATE_TIME_FORMATTER = SimpleDateFormat(DATE_TIME_PATTERN, Locale.ENGLISH)
    private val DAY_DATE_FORMATTER = SimpleDateFormat(DAY_DATE_PATTERN, Locale.ENGLISH)
    private val DATE_FORMATTER = SimpleDateFormat(DATE_PATTERN, Locale.ENGLISH)
    private val TIME_FORMATTER = SimpleDateFormat(TIME_PATTERN, Locale.ENGLISH)
    private val CHAT_DATE_FORMATTER = SimpleDateFormat(CHAT_DATE_PATTERN, Locale.ENGLISH)
    private val CHAT_DATE_YEARLESS_FORMATTER = SimpleDateFormat(CHAT_DATE_YEARLESS_PATTERN, Locale.ENGLISH)

    fun getCurrentDateTimeStringForDb(): String {
        return DATE_TIME_FORMATTER.format(Date())
    }

    fun parseForRiwayatDeteksi(dateTime: String): String {
        val converted = parseDateTime(dateTime)
        return DAY_DATE_FORMATTER.format(converted)
    }

    fun parseForDisplayedMessage(milis: Long): String {
        val date = Date(milis)
        val dateTime = DATE_TIME_FORMATTER.format(date)
        val now = DATE_FORMATTER.format(Date())
        val dateOnly = dateTime.substringBefore(" ")
        val currentYear = now.substringBefore("-")
        val yearOnly = dateOnly.substringBefore("-")
        val converted = parseDateTime(dateTime)

        return when {
            dateOnly == now -> TIME_FORMATTER.format(converted)
            yearOnly == currentYear -> CHAT_DATE_YEARLESS_FORMATTER.format(converted)
            else -> CHAT_DATE_FORMATTER.format(converted)
        }
    }

    fun parseForDisplayedMessage(dateTime: String): String {
        val now = DATE_FORMATTER.format(Date())
        val dateOnly = dateTime.substringBefore(" ")
        val currentYear = now.substringBefore("-")
        val yearOnly = dateOnly.substringBefore("-")
        val converted = parseDateTime(dateTime)

        return when {
            dateOnly == now -> TIME_FORMATTER.format(converted)
            yearOnly == currentYear -> CHAT_DATE_YEARLESS_FORMATTER.format(converted)
            else -> CHAT_DATE_FORMATTER.format(converted)
        }
    }

    private fun parseDateTime(dateTime: String) = DATE_TIME_FORMATTER.parse(dateTime)
}