package um.fik.ikm.dmpersonalscreening.konsultasi.contact

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_contact.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.konsultasi.chat.ChatActivity
import um.fik.ikm.dmpersonalscreening.model.User
import um.fik.ikm.dmpersonalscreening.panduan.ContactUsActivity
import um.fik.ikm.dmpersonalscreening.profil.ProfilActivity
import um.fik.ikm.dmpersonalscreening.util.InjectorUtils

class ContactActivity : AppCompatActivity(), ContactContract.View {

    override lateinit var presenter: ContactContract.Presenter
    private val rvAdapter = ContactRvAdapter(
            { showChatPage(it) },
            { showProfilPage(it) }
    )

    companion object {
        fun intent(context: Context) = Intent(context, ContactActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)

        presenter = ContactPresenter(this, InjectorUtils.provideUserManager())

        with(swipe_refresh) {
            setColorSchemeResources(R.color.colorAccent)
            setOnRefreshListener { presenter.loadContactList() }
        }
        list_person.adapter = rvAdapter

        fab_contact_us.setOnClickListener { startActivity(ContactUsActivity.intent(this)) }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun showContactList(list: List<User>) {
        rvAdapter.list = list
    }

    override fun showToolbarSubtitle(message: String) {
        supportActionBar?.subtitle = message
    }

    override fun showChatPage(user: User) {
        startActivity(ChatActivity.intent(this, user))
        finish()
    }

    override fun showProfilPage(user: User) {
        startActivity(ProfilActivity.intent(this, user))
    }

    override fun onLoading(active: Boolean) {
        swipe_refresh.isRefreshing = active
    }

    override fun onError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

}
