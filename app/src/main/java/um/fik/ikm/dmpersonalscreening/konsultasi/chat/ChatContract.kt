package um.fik.ikm.dmpersonalscreening.konsultasi.chat

import android.net.Uri
import um.fik.ikm.dmpersonalscreening.model.Message

interface ChatContract {

    interface View {
        fun onRetrieveAll(list: List<Message>)
        fun onChatAdded(message: Message)
        fun onSendMessageSuccess()
        fun changeStatusUser(isOnline: Boolean)
        fun pickMedia()
        fun onImageAdded(uri: Uri)
        fun onVideoAdded(uri: Uri)
        fun onImageRemoved()
        fun onVideoRemoved()
        fun viewAddedImage()
        fun playAddedVideo()
        fun viewImage(selectedUrl: String)
        fun playVideo(selectedUrl: String)
        fun onUploadLoading(active: Boolean)
        fun onChatLoading(active: Boolean)
        fun onError(message: String)
        fun sendMessage()
    }

    interface Presenter {
        fun loadAll()
        fun setListener()
        fun removeListener()
        fun pushMessageNode(message: Message, addedImage: Uri? = null, addedVideo: Uri? = null)
    }
}