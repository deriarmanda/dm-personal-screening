package um.fik.ikm.dmpersonalscreening.model

data class MessageHistory(
        var user: User = User(),
        var lastMessage: Message = Message()
)