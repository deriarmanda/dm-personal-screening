package um.fik.ikm.dmpersonalscreening.konsultasi.chat

import android.Manifest
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.robertlevonyan.components.picker.ItemModel
import com.robertlevonyan.components.picker.PickerDialog
import com.stfalcon.imageviewer.StfalconImageViewer
import kotlinx.android.synthetic.main.activity_chat.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.deteksi.DeteksiActivity
import um.fik.ikm.dmpersonalscreening.deteksi.hasil.HasilDeteksiActivity.Constants.HASIL_NONE
import um.fik.ikm.dmpersonalscreening.konsultasi.chat.video.VideoActivity
import um.fik.ikm.dmpersonalscreening.model.Message
import um.fik.ikm.dmpersonalscreening.model.User
import um.fik.ikm.dmpersonalscreening.profil.ProfilActivity
import um.fik.ikm.dmpersonalscreening.util.InjectorUtils

class ChatActivity : AppCompatActivity(), ChatContract.View {

    private val rvAdapter = ChatRvAdapter(
            onImageClicked = { viewImage(it) },
            onVideoClicked = { playVideo(it) }
    )
    private lateinit var presenter: ChatPresenter
    private lateinit var user: User
    private lateinit var mediaPicker: PickerDialog
    private var uriAddedImage: Uri? = null
    private var uriAddedVideo: Uri? = null

    companion object {
        private const val EXTRA_USER = "extra_user"
        fun intent(context: Context, user: User): Intent {
            val intent = Intent(context, ChatActivity::class.java)
            intent.putExtra(EXTRA_USER, user)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        Dexter.withActivity(this).withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).withListener(object : MultiplePermissionsListener {
            override fun onPermissionsChecked(report: MultiplePermissionsReport?) {}
            override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {}
        }).check()

        user = intent.getParcelableExtra(EXTRA_USER)
        supportActionBar?.title = user.name

        presenter = InjectorUtils.provideChatPresenter(user, this)

        list_chat.adapter = rvAdapter
        with(swipe_refresh) {
            setColorSchemeResources(R.color.colorAccent)
            setOnRefreshListener { presenter.loadAll() }
        }
        image_send.setOnClickListener { sendMessage() }

        prepareMediaPicker()
        image_add_media.setOnClickListener { pickMedia() }
        image_added_picture.setOnClickListener { viewAddedImage() }
        box_video.setOnClickListener { playAddedVideo() }
        image_remove_picture.setOnClickListener { onImageRemoved() }
        image_remove_video.setOnClickListener { onVideoRemoved() }

        presenter.loadAll()
    }

    override fun onStart() {
        super.onStart()
        presenter.setListener()
    }

    override fun onStop() {
        super.onStop()
        presenter.removeListener()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mediaPicker.onPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_chat, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.option_show_profile -> {
                startActivity(ProfilActivity.intent(this, user))
                true
            }
            R.id.option_show_deteksi -> {
                if (user.hasilDeteksi == HASIL_NONE) {
                    Toast.makeText(this, R.string.msg_empty_deteksi, Toast.LENGTH_LONG).show()
                } else {
                    startActivity(DeteksiActivity.intent(this, user, false))
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onRetrieveAll(list: List<Message>) {
//        rvAdapter.setMessages(list)
        for (msg in list) rvAdapter.addMessage(msg)
    }

    override fun onChatAdded(message: Message) {
        rvAdapter.addMessage(message)
    }

    override fun onSendMessageSuccess() {
        form_message.setText("")
        onUploadLoading(false)
        onImageRemoved()
        onVideoRemoved()
    }

    override fun changeStatusUser(isOnline: Boolean) {
        supportActionBar?.subtitle =
                if (isOnline) getString(R.string.msg_user_online)
                else ""
    }

    override fun pickMedia() {
        mediaPicker.show(supportFragmentManager, "")
    }

    override fun onImageAdded(uri: Uri) {
        uriAddedImage = uri
        container_image.visibility = View.VISIBLE
        Glide.with(this).load(uri).into(image_added_picture)
        onVideoRemoved()
    }

    override fun onVideoAdded(uri: Uri) {
        uriAddedVideo = uri
        container_video.visibility = View.VISIBLE
        text_added_video.text = uri.lastPathSegment
        onImageRemoved()
    }

    override fun onImageRemoved() {
        uriAddedImage = null
        container_image.visibility = View.GONE
    }

    override fun onVideoRemoved() {
        uriAddedVideo = null
        container_video.visibility = View.GONE
    }

    override fun viewAddedImage() {
        uriAddedImage?.let {
            StfalconImageViewer.Builder<Uri>(this, listOf(it)) { view, uri ->
                Glide.with(view.context).load(uri).into(view)
            }.show()
        }
    }

    override fun playAddedVideo() {
        uriAddedVideo?.let { startActivity(VideoActivity.intent(this, it)) }
    }

    override fun viewImage(selectedUrl: String) {
        StfalconImageViewer.Builder<String>(this, listOf(selectedUrl)) { view, url ->
            Glide.with(view.context).load(url).into(view)
        }.show()
    }

    override fun playVideo(selectedUrl: String) {
        startActivity(VideoActivity.intent(this, selectedUrl))
    }

    override fun onUploadLoading(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }

    override fun onChatLoading(active: Boolean) {
        swipe_refresh.isRefreshing = active
    }

    override fun onError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun sendMessage() {
        val message = Message(
                message = form_message.text.toString().trim(),
                timestamp = System.currentTimeMillis()
        )
        presenter.pushMessageNode(message, uriAddedImage, uriAddedVideo)
    }

    private fun prepareMediaPicker() {
        val mediaItems = arrayListOf(
                ItemModel(ItemModel.ITEM_CAMERA, getString(R.string.action_take_photo), R.drawable.ic_camera_alt_white_24dp, true, ItemModel.TYPE_CIRCLE),
                ItemModel(ItemModel.ITEM_GALLERY, getString(R.string.action_choose_photo), R.drawable.ic_image_white_24dp, true, ItemModel.TYPE_CIRCLE),
                ItemModel(ItemModel.ITEM_VIDEO, getString(R.string.action_take_video), R.drawable.ic_videocam_white_24dp, true, ItemModel.TYPE_CIRCLE),
                ItemModel(ItemModel.ITEM_VIDEO_GALLERY, getString(R.string.action_choose_video), R.drawable.ic_video_library_white_24dp, true, ItemModel.TYPE_CIRCLE)
        )

        mediaPicker = PickerDialog.Builder(this)
                .setTitle(R.string.action_select_media)
                .setTitleTextSize(20f)
                .setListType(PickerDialog.TYPE_GRID)
                .setItems(mediaItems)
                .setDialogStyle(PickerDialog.DIALOG_MATERIAL)
                .create()

        mediaPicker.setPickerCloseListener { type, uri ->
            when (type) {
                ItemModel.ITEM_CAMERA, ItemModel.ITEM_GALLERY -> onImageAdded(uri)
                ItemModel.ITEM_VIDEO, ItemModel.ITEM_VIDEO_GALLERY -> onVideoAdded(uri)
            }
        }
    }
}
