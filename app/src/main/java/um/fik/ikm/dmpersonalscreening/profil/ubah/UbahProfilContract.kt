package um.fik.ikm.dmpersonalscreening.profil.ubah

import um.fik.ikm.dmpersonalscreening.BasePresenter
import um.fik.ikm.dmpersonalscreening.BaseView
import um.fik.ikm.dmpersonalscreening.model.User

interface UbahProfilContract {

    interface Presenter : BasePresenter {
        fun saveEditedProfil(user: User)
    }

    interface View : BaseView<Presenter> {
        fun setCurrentProfil(user: User)
        fun validateForm()
        fun onSaveProfilSucceed()
        fun onSaveProfilError(message: String)
        fun onLoading(active: Boolean)
    }
}