package um.fik.ikm.dmpersonalscreening.model

import com.google.firebase.database.Exclude

data class Quest (
        var question: String = "",
        var answer: String = "tidak",
        @get:Exclude var score: Int = 0,
        @get:Exclude var alreadyChecked: Boolean = false
)