package um.fik.ikm.dmpersonalscreening.profil

import um.fik.ikm.dmpersonalscreening.deteksi.hasil.HasilDeteksiActivity.Constants.HASIL_NONE
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.util.PrefManager

class ProfilPresenter(
        val view: ProfilContract.View,
        private val userManager: UserManager
) : ProfilContract.Presenter {

    private val prefManager: PrefManager

    init {
        view.presenter = this
        prefManager = PrefManager((view as ProfilFragment).context!!)
    }

    override fun loadProfil() {
        val user = userManager.getActiveUser()
        view.showProfil(user)
        if (user.hasilDeteksi == HASIL_NONE) view.showEmptyDeteksiHistory()
        else view.showDeteksiHistory()
    }

    override fun editProfil() {
        view.showUbahProfilPage()
    }

    override fun loadDeteksiHistory() {
        val user = userManager.getActiveUser()
        view.showHasilDeteksiPage(user)
    }

    override fun start() {
        loadProfil()
    }
}