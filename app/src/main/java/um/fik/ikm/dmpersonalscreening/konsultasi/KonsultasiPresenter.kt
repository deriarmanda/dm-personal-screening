package um.fik.ikm.dmpersonalscreening.konsultasi

import um.fik.ikm.dmpersonalscreening.manager.MessageManager
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.Message
import um.fik.ikm.dmpersonalscreening.model.MessageHistory
import um.fik.ikm.dmpersonalscreening.model.User

class KonsultasiPresenter(
        private val view: KonsultasiContract.View,
        private val messageManager: MessageManager,
        private val userManager: UserManager
) : KonsultasiContract.Presenter {

    private val currentUser: User
    private var stopped: Boolean = false

    init {
        view.presenter = this
        currentUser = userManager.getActiveUser()
    }

    override fun loadChatHistory() {
        view.onLoading(true)
        if (currentUser.role == UserManager.USER_COMMON_ROLE) {
            messageManager.getUserHistoryList(currentUser.uid) { list, error ->
                if (error != null && !stopped) {
                    view.onLoading(false)
                    view.onError(error)
                }
                if (list != null) {
                    if (list.isEmpty() && !stopped) {
                        view.onLoading(false)
                        view.showEmptyHistoryMessage()
                    } else {
                        val history = arrayListOf<MessageHistory>()
                        for ((index, uid) in list.withIndex()) {
                            userManager.findUserByUid(uid) { user, _ ->
                                messageManager.findLatestMessage(uid, currentUser.uid) { message ->
                                    if (user != null) {
                                        history.add(MessageHistory(user, message ?: Message()))
                                    }
                                    if (index == list.size - 1 && !stopped) {
                                        view.onLoading(false)
                                        view.showChatHistoryList(history)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            messageManager.getAhliHistoryList(currentUser.uid) { list, error ->
                if (error != null && !stopped) {
                    view.onLoading(false)
                    view.onError(error)
                }
                if (list != null) {
                    if (list.isEmpty() && !stopped) {
                        view.onLoading(false)
                        view.showEmptyHistoryMessage()
                    } else {
                        val history = arrayListOf<MessageHistory>()
                        for ((index, uid) in list.withIndex()) {
                            userManager.findUserByUid(uid) { user, _ ->
                                messageManager.findLatestMessage(currentUser.uid, uid) { message ->
                                    if (user != null) {
                                        history.add(MessageHistory(user, message ?: Message()))
                                    }
                                    if (index == list.size - 1 && !stopped) {
                                        view.onLoading(false)
                                        view.showChatHistoryList(history)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun start() {
        stopped = false
        loadChatHistory()
    }

    override fun stop() {
        stopped = true
    }
}