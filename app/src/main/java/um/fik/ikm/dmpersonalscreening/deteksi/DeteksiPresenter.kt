package um.fik.ikm.dmpersonalscreening.deteksi

import com.google.firebase.database.*
import um.fik.ikm.dmpersonalscreening.deteksi.hasil.HasilDeteksiActivity
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.Quest
import um.fik.ikm.dmpersonalscreening.model.User
import um.fik.ikm.dmpersonalscreening.util.DateTimeUtils

class DeteksiPresenter(
        val view: DeteksiContract.View,
        private val user: User,
        private val maxScore: Int
) : DeteksiContract.Presenter {

    private val mDatabase: DatabaseReference
    private val userManager = UserManager.getInstance()
    private var totalScore: Float

    init {
        view.presenter = this
        mDatabase = FirebaseDatabase.getInstance().getReference("/kuisioner/${user.uid}")
        totalScore = 0f
    }

    override fun prepareRiwayatDeteksi() {
        view.onLoading(true)
        mDatabase.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                view.onLoading(false)
                view.onError("Gagal memuat data riwayat deteksi, silahkan coba lagi.\n(${p0.message})")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val answerMap = hashMapOf(
                            Pair("aspek_umum", arrayListOf<Quest>()),
                            Pair("aspek_gigi", arrayListOf()),
                            Pair("aspek_pola_makan", arrayListOf()),
                            Pair("aspek_fisik", arrayListOf())
                    )
                    for (key in answerMap.keys) {
                        val dbRef = snapshot.child(key)
                        for (child in dbRef.children) {
                            val data = child.getValue(Quest::class.java)
                            if (data != null) answerMap[key]?.add(data)
                        }
                    }
                    view.onLoading(false)
                    view.onShowRiwayatDeteksi(answerMap)
                } else {
                    view.onLoading(false)
                    view.onError("Pengguna tidak memiliki riwayat deteksi.")
                }
            }
        })
    }

    override fun updateScore(score: Int) {
        totalScore += score
    }

    override fun calculateScore(answerMap: HashMap<String, List<Quest>>) {
        view.onLoading(true)
        var currentScore: Float = totalScore / maxScore
        currentScore *= 100

        val hasil = when (currentScore) {
            in 0f..25f -> HasilDeteksiActivity.Constants.HASIL_1
            in 26f..50f -> HasilDeteksiActivity.Constants.HASIL_2
            in 51f..75f -> HasilDeteksiActivity.Constants.HASIL_3
            else -> HasilDeteksiActivity.Constants.HASIL_4
        }

        user.hasilDeteksi = hasil
        user.tanggalDeteksi = DateTimeUtils.getCurrentDateTimeStringForDb()

        mDatabase.setValue(answerMap) { dbError, _ ->
            view.onLoading(false)
            if (dbError != null) view.onError("Gagal menghitung skor, silahkan coba lagi.\n(${dbError.message})")
            else {
                userManager.updateCurrentUserProfile(user) {
                    if (it != null) view.onError("Gagal menghitung skor, silahkan coba lagi.\n($it)")
                    else view.showResultPage(hasil)
                }
            }
        }
    }

    override fun start() {}
}