package um.fik.ikm.dmpersonalscreening.manager

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.fik.ikm.dmpersonalscreening.model.User

class UserManager private constructor() {

    private val mDatabase = FirebaseDatabase.getInstance().getReference("/users")
    private var mUser: User = User()

    companion object {
        const val USER_ONLINE = 1
        const val USER_OFFLINE = 0
        const val USER_COMMON_ROLE = "Pengguna"
        private const val TAG = "UserManager"
        @Volatile
        private var instance: UserManager? = null

        fun getInstance(): UserManager {
            return instance ?: synchronized(this) {
                instance ?: UserManager().also { instance = it }
            }
        }
    }

    fun getActiveUser() = mUser

    fun loadUserProfile(firebaseUser: FirebaseUser, callback: ((errorMessage: String?) -> Unit)) {
        val dbRef = mDatabase.child(firebaseUser.uid)
        dbRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) = callback(p0.message)

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    val user = p0.getValue(User::class.java)
                    if (user != null) {
                        user.uid = p0.key ?: firebaseUser.uid
                        mUser = user.copy()
                        setUserOffline()
                    }
//                    updateUserStatus(USER_ONLINE)
                    callback(null)
                } else {
                    mUser.name = firebaseUser.displayName ?: firebaseUser.email!!
                    mUser.imageUrl = firebaseUser.photoUrl?.toString() ?: ""
                    mUser.role = USER_COMMON_ROLE
//                    mUser.status = USER_ONLINE
                    dbRef.setValue(mUser) { dbError, _ ->
                        mUser.uid = firebaseUser.uid
                        setUserOffline()
                        callback(dbError?.message)
                    }
                }
            }
        })
    }

    fun updateCurrentUserProfile(user: User, callback: (errorMessage: String?) -> Unit) {
        val dbRef = mDatabase.child(mUser.uid)
        dbRef.setValue(user) { error, _ ->
            mUser = user.copy()
            callback(error?.message)
        }
    }

    fun updateUserProfile(user: User, callback: (errorMessage: String?) -> Unit) {
        val dbRef = mDatabase.child(user.uid)
        dbRef.setValue(user) { error, _ ->
            callback(error?.message)
        }
    }

    fun getAllUsers(callback: (list: List<User>?, errorMsg: String?) -> Unit) {
        mDatabase.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) = callback(null, p0.message)
            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    val list = arrayListOf<User>()
                    for (data in p0.children) {
                        val user = data.getValue(User::class.java)
                        if (user != null) {
                            user.uid = data.key ?: ""
                            if (user.uid != mUser.uid) list.add(user)
                        }
                    }
                    list.sortByDescending { it.role }
                    callback(list, null)
                } else callback(emptyList(), null)
            }
        })
    }

    fun getAhliUsersOnly(callback: (list: List<User>?, errorMsg: String?) -> Unit) {
        mDatabase.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) = callback(null, p0.message)
            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    val list = arrayListOf<User>()
                    for (data in p0.children) {
                        val user = data.getValue(User::class.java)
                        if (user != null) {
                            user.uid = data.key ?: ""
                            if (user.uid != mUser.uid &&
                                    user.role != USER_COMMON_ROLE
                            ) list.add(user)
                        }
                    }
                    list.sortBy { it.role }
                    callback(list, null)
                } else callback(emptyList(), null)
            }
        })
    }

    fun findUserByUid(uid: String, callback: ((user: User?, error: String?) -> Unit)) {
        val dbRef = mDatabase.child(uid)
        dbRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) = callback(null, p0.message)
            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    val user = p0.getValue(User::class.java)
                    if (user != null) {
                        user.uid = p0.key ?: uid
                        callback(user, null)
                    } else callback(null, null) //callback(null, "Error while parsing user objects.")
                } else callback(null, null) //callback(null, "User with $uid doesnt exists.")
            }
        })
    }

    fun prepareUserStatusListener() {
        val connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected")
        connectedRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val connected = snapshot.getValue(Boolean::class.java) ?: false
                if (connected) setUserOnline()
                else setUserOffline()
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }

    fun setUserOnline() {
        val user = mUser.copy(status = USER_ONLINE)
        mDatabase.child(user.uid).setValue(user) { _, _ ->
            mUser.status = USER_ONLINE
            Log.d(TAG, "onOnline -> uid: ${mUser.uid}, name: ${mUser.name}, status: ${mUser.status}")
        }
    }

    fun setUserOffline() {
        val user = mUser.copy(status = USER_OFFLINE)
        mDatabase.child(user.uid).onDisconnect().setValue(user) { err, _ ->
            mUser.status = USER_OFFLINE
            Log.d(TAG, "onOffline -> uid: ${mUser.uid}, name: ${mUser.name}, status: ${mUser.status} -> error: ${err?.message}")
        }
    }

    fun doLogout() {
        setUserOffline()
        mUser = User()
        FirebaseAuth.getInstance().signOut()
    }
}