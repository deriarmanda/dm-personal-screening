package um.fik.ikm.dmpersonalscreening.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude
import um.fik.ikm.dmpersonalscreening.deteksi.hasil.HasilDeteksiActivity
import um.fik.ikm.dmpersonalscreening.manager.UserManager

data class User(
        var name: String = "",
        var role: String = UserManager.USER_COMMON_ROLE,
        var imageUrl: String = "",
        var usia: String = "",
        var jenisKelamin: String = "",
        var alamat: String = "",
        var pekerjaan: String = "",
        var pendidikan: String = "",
        var hasilDeteksi: Int = HasilDeteksiActivity.Constants.HASIL_NONE,
        var tanggalDeteksi: String = "",
        var status: Int = UserManager.USER_OFFLINE,
        @get:Exclude var uid: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString() ?: "",
            parcel.readString() ?: UserManager.USER_COMMON_ROLE,
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readInt(),
            parcel.readString() ?: "",
            parcel.readInt(),
            parcel.readString() ?: "")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(role)
        parcel.writeString(imageUrl)
        parcel.writeString(usia)
        parcel.writeString(jenisKelamin)
        parcel.writeString(alamat)
        parcel.writeString(pekerjaan)
        parcel.writeString(pendidikan)
        parcel.writeInt(hasilDeteksi)
        parcel.writeString(tanggalDeteksi)
        parcel.writeInt(status)
        parcel.writeString(uid)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}