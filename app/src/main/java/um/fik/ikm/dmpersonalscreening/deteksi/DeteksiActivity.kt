package um.fik.ikm.dmpersonalscreening.deteksi

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import kotlinx.android.synthetic.main.activity_deteksi.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.deteksi.adapter.FormFragmentAdapter
import um.fik.ikm.dmpersonalscreening.deteksi.hasil.HasilDeteksiActivity
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.Quest
import um.fik.ikm.dmpersonalscreening.model.User
import um.fik.ikm.dmpersonalscreening.util.DateTimeUtils
import um.fik.ikm.dmpersonalscreening.util.getFormDeteksiFragment

class DeteksiActivity : AppCompatActivity(), DeteksiContract.View {

    override lateinit var presenter: DeteksiContract.Presenter
    private lateinit var fragmentAdapter: FormFragmentAdapter
    private lateinit var confirmDialog: AlertDialog
    private var currentUser = User()
    private var isShowKuisioner = true

    companion object {
        private const val EXTRA_USER = "extra_user"
        private const val EXTRA_FLAG = "extra_flag"
        fun intent(context: Context): Intent {
            val intent = Intent(context, DeteksiActivity::class.java)
            val user = UserManager.getInstance().getActiveUser()
            intent.putExtra(EXTRA_USER, user)
            return intent
        }

        fun intent(context: Context, user: User, flag: Boolean): Intent {
            val intent = Intent(context, DeteksiActivity::class.java)
            intent.putExtra(EXTRA_USER, user)
            intent.putExtra(EXTRA_FLAG, flag)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deteksi)

        currentUser = intent.getParcelableExtra(EXTRA_USER)
        isShowKuisioner = intent.getBooleanExtra(EXTRA_FLAG, true)
        if (isShowKuisioner) {
            toolbar.setTitle(R.string.title_kuisioner)
        } else {
            toolbar.setTitle(R.string.title_riwayat_deteksi)
            toolbar.subtitle = DateTimeUtils.parseForRiwayatDeteksi(currentUser.tanggalDeteksi)
        }
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter = DeteksiPresenter(this, currentUser, resources.getInteger(R.integer.total_score))

        if (isShowKuisioner) onShowKuisioner()
        else presenter.prepareRiwayatDeteksi()

        button_hasil.setOnClickListener {
            if (isShowKuisioner) submitScore()
            else showResultPage(currentUser.hasilDeteksi)
        }

        val coloredTitle = SpannableString(getString(R.string.title_confirm_dialog))
        coloredTitle.setSpan(
                ForegroundColorSpan(ResourcesCompat.getColor(resources, R.color.colorPrimary, theme)),
                0,
                coloredTitle.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        confirmDialog = AlertDialog.Builder(this)
                .setTitle(coloredTitle)
                .setIcon(R.drawable.ic_confirm_dialog_blue_24dp)
                .setMessage(R.string.text_content_confirm_dialog)
                .setCancelable(false)
                .setPositiveButton(R.string.btn_ya) { _, _ ->
                    finish()
                }
                .setNegativeButton(R.string.btn_tidak) { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
                .create()
    }

    override fun onBackPressed() {
        if (isShowKuisioner) confirmDialog.show()
        else super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onShowKuisioner() {
        fragmentAdapter = FormFragmentAdapter(
                supportFragmentManager,
                resources.getStringArray(R.array.title_form_fragments),
                getFormDeteksiFragment(emptyMap())
        )
        view_pager.adapter = fragmentAdapter
        view_pager.offscreenPageLimit = 5

        tab_layout.setupWithViewPager(view_pager)
        button_hasil.setText(R.string.btn_selesai)
    }

    override fun onShowRiwayatDeteksi(map: Map<String, List<Quest>>) {
        fragmentAdapter = FormFragmentAdapter(
                supportFragmentManager,
                resources.getStringArray(R.array.title_form_fragments),
                getFormDeteksiFragment(map)
        )
        view_pager.adapter = fragmentAdapter
        view_pager.offscreenPageLimit = 5

        tab_layout.setupWithViewPager(view_pager)
        button_hasil.setText(R.string.btn_lihat_hasil)
    }

    override fun onShowRiwayatError(message: String) {
        AlertDialog.Builder(this)
                .setTitle(R.string.title_error)
                .setIcon(R.drawable.ic_warning_accent_24dp)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.action_retry) { _, _ ->
                    presenter.prepareRiwayatDeteksi()
                }
                .setNegativeButton(R.string.action_kembali) { _, _ ->
                    finish()
                }
                .create()
                .show()
    }

    override fun onScoreUpdate(score: Int) {
        presenter.updateScore(score)
    }

    override fun submitScore() {
        val fragUmum = fragmentAdapter.getItem(0) as FragmentListener
        val fragGigi = fragmentAdapter.getItem(1) as FragmentListener
        val fragPolaMakan = fragmentAdapter.getItem(2) as FragmentListener
        val fragFisik = fragmentAdapter.getItem(3) as FragmentListener
        val map = hashMapOf(
                Pair("aspek_umum", fragUmum.getQuestList()),
                Pair("aspek_gigi", fragGigi.getQuestList()),
                Pair("aspek_pola_makan", fragPolaMakan.getQuestList()),
                Pair("aspek_fisik", fragFisik.getQuestList())
        )

        presenter.calculateScore(map)
    }

    override fun showResultPage(hasil: Int) {
        startActivity(HasilDeteksiActivity.intent(this, hasil))
        if (isShowKuisioner) finish()
    }

    override fun onError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onLoading(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
        button_hasil.isEnabled = !active
    }

    interface FragmentListener {
        fun getQuestList(): List<Quest>
    }
}
