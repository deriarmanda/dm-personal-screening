package um.fik.ikm.dmpersonalscreening.konsultasi.chat.video

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_video.*
import um.fik.ikm.dmpersonalscreening.R
import java.io.File

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class VideoActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_URL = "extra_url"
        fun intent(context: Context, uri: Uri): Intent {
            val intent = Intent(context, VideoActivity::class.java)
            intent.data = uri
            return intent
        }

        fun intent(context: Context, url: String): Intent {
            val intent = Intent(context, VideoActivity::class.java)
            intent.putExtra(EXTRA_URL, url)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        val uri = intent.data
        if (uri != null) video_view.setSource(uri)
        else {
            onLoading(true)
            val url = intent.getStringExtra(EXTRA_URL)
            val ref = FirebaseStorage.getInstance().getReferenceFromUrl(url)
            ref.metadata.addOnCompleteListener {
                if (it.isSuccessful) {
                    val result = it.result
                    if (result != null) {
                        val name = result.name ?: "video_file"
                        val type = result.contentType.substringAfter("/")
                        val localFile = File.createTempFile(name, type)
                        downloadFile(ref, localFile)
                    }
                } else {
                    onLoading(false)
                    onError()
                    finish()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        video_view.start()
    }

    override fun onStop() {
        super.onStop()
        video_view.pause()
    }

    private fun downloadFile(reference: StorageReference, localFile: File) {
        reference.getFile(localFile).addOnCompleteListener {
            onLoading(false)
            if (it.isSuccessful) {
                val uri = Uri.fromFile(localFile)
                video_view.setSource(uri)
            } else {
                onError()
                finish()
            }
        }
    }

    private fun onError(msg: String = "") {
        if (msg.isEmpty()) Toast.makeText(this, R.string.error_load_video, Toast.LENGTH_LONG).show()
        else Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }

    private fun onLoading(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }
}
