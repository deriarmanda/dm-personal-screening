package um.fik.ikm.dmpersonalscreening.konsultasi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_history.view.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.MessageHistory
import um.fik.ikm.dmpersonalscreening.model.User
import um.fik.ikm.dmpersonalscreening.util.DateTimeUtils

class KonsultasiRvAdapter(
        private val onItemClick: ((MessageHistory) -> Unit),
        private val onImageClick: ((User) -> Unit)
) : androidx.recyclerview.widget.RecyclerView.Adapter<KonsultasiRvAdapter.ViewHolder>() {

    var list: List<MessageHistory> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context)
                .inflate(R.layout.item_history, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) = p0.fetch(list[p1])

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(history: MessageHistory) {
            itemView.text_nama.text = history.user.name
            itemView.text_pesan.text = history.lastMessage.message

            val date = history.lastMessage.timestamp
            if (date != 0L) {
                itemView.text_date.text = DateTimeUtils.parseForDisplayedMessage(date)
                itemView.text_pesan.visibility = View.VISIBLE
            } else {
                itemView.text_date.text = ""
                itemView.text_pesan.visibility = View.GONE
            }

            if (history.user.status == UserManager.USER_ONLINE) {
                itemView.text_date.setText(R.string.msg_user_online)
                itemView.text_date.setTextColor(ResourcesCompat.getColor(itemView.resources, R.color.colorAccent, itemView.context.theme))
            } else {
                itemView.text_date.setTextColor(ResourcesCompat.getColor(itemView.resources, R.color.secondary_text, itemView.context.theme))
            }

            if (history.user.imageUrl.isEmpty()) {
                itemView.image_profile.setImageResource(R.drawable.ic_face_gray_64dp)
            } else {
                Glide.with(itemView)
                        .load(history.user.imageUrl)
                        .into(itemView.image_profile)
            }

            itemView.image_profile.setOnClickListener { onImageClick(history.user) }
            itemView.root.setOnClickListener { onItemClick(history) }
        }
    }
}