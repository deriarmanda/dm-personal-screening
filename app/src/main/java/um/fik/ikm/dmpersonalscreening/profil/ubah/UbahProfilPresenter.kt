package um.fik.ikm.dmpersonalscreening.profil.ubah

import um.fik.ikm.dmpersonalscreening.manager.UserManager
import um.fik.ikm.dmpersonalscreening.model.User

class UbahProfilPresenter(
        val view: UbahProfilContract.View,
        private val userManager: UserManager
) : UbahProfilContract.Presenter {

    init {
        val user = userManager.getActiveUser()
        view.presenter = this
        view.setCurrentProfil(user)
    }

    override fun saveEditedProfil(user: User) {
        view.onLoading(true)
        userManager.updateCurrentUserProfile(user) {
            view.onLoading(false)
            if (it != null) view.onSaveProfilError(it)
            else view.onSaveProfilSucceed()
        }
    }

    override fun start() {
//        view.setCurrentProfil()
    }
}