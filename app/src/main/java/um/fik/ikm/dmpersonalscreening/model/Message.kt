package um.fik.ikm.dmpersonalscreening.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude
import um.fik.ikm.dmpersonalscreening.manager.UserManager

data class Message (
        var message: String = "",
        var timestamp: Long = 0,
        var imageUrl: String = "",
        var videoUrl: String = "",
        var role: String = UserManager.USER_COMMON_ROLE,
        @get:Exclude var uid: String = "",
        @get:Exclude var isSelfMessage: Boolean = false
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString() ?: "",
            parcel.readLong(),
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(message)
        parcel.writeLong(timestamp)
        parcel.writeString(imageUrl)
        parcel.writeString(videoUrl)
        parcel.writeString(role)
        parcel.writeString(uid)
        parcel.writeByte(if (isSelfMessage) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other is Message
                && other.uid.isNotEmpty() && this.uid.isNotEmpty()
                && other.uid == this.uid
    }

    override fun hashCode(): Int {
        var result = message.hashCode()
        result = 31 * result + timestamp.hashCode()
        result = 31 * result + imageUrl.hashCode()
        result = 31 * result + videoUrl.hashCode()
        result = 31 * result + role.hashCode()
        result = 31 * result + uid.hashCode()
        result = 31 * result + isSelfMessage.hashCode()
        return result
    }

    companion object CREATOR : Parcelable.Creator<Message> {
        override fun createFromParcel(parcel: Parcel): Message {
            return Message(parcel)
        }

        override fun newArray(size: Int): Array<Message?> {
            return arrayOfNulls(size)
        }
    }
}