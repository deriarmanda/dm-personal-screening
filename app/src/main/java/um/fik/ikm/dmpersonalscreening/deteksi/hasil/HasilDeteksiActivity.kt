package um.fik.ikm.dmpersonalscreening.deteksi.hasil

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import kotlinx.android.synthetic.main.activity_hasil_deteksi.*
import um.fik.ikm.dmpersonalscreening.R
import um.fik.ikm.dmpersonalscreening.util.PrefManager

class HasilDeteksiActivity : AppCompatActivity() {

    object Constants {
        const val HASIL_NONE = 210
        const val HASIL_1 = 211
        const val HASIL_2 = 212
        const val HASIL_3 = 213
        const val HASIL_4 = 214
    }

    companion object {
        private const val EXTRA_HASIL = "extra_hasil"
        fun intent(context: Context, hasil: Int): Intent {
            val intent = Intent(context, HasilDeteksiActivity::class.java)
            intent.putExtra(EXTRA_HASIL, hasil)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hasil_deteksi)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        btn_selesai.setOnClickListener { finish() }

        val hasil = intent.getIntExtra(EXTRA_HASIL, Constants.HASIL_NONE)
        setupView(hasil)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupView(hasil: Int) {
        val color: Int
        val percent: Int
        val recommend: Int

        when (hasil) {
            Constants.HASIL_1 -> {
                color = R.color.lightGreen
                percent = R.string.text_percent_1
                recommend = R.string.text_rekomendasi_1
            }
            Constants.HASIL_2 -> {
                color = R.color.mediumGreen
                percent = R.string.text_percent_2
                recommend = R.string.text_rekomendasi_2
            }
            Constants.HASIL_3 -> {
                color = R.color.mediumRed
                percent = R.string.text_percent_3
                recommend = R.string.text_rekomendasi_3
            }
            Constants.HASIL_4 -> {
                color = R.color.darkRed
                percent = R.string.text_percent_4
                recommend = R.string.text_rekomendasi_4
            }
            else -> return
        }

        //card_hasil.setCardBackgroundColor(color)
        card_hasil.setCardBackgroundColor(ResourcesCompat.getColor(resources, color, theme))
        text_hasil.setText(percent)
        text_rekomendasi.setText(recommend)

        val prefManager = PrefManager(this)
        prefManager.updateProfilDeteksi(hasil)
    }
}
